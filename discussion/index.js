// console.log('Hello From JS');

//DOM Selectors


//first you have to describe the location where in its going to target/access elements.

//identify the 'attribute' and 'value' to properly recognize and identify which element to target.
//visualize the statement in JS
//let/const jsObject = {}

//[SECTION] querySelector
// const firstName = document.querySelector('#firstName');
// const lastName = document.querySelector('#lastName');

//[SECTION] getElement Functions
//getElementById
const firstName = document.getElementById('firstName');
const lastName = document.getElementById('lastName');

//getElementsByClassName => can be essential when targeting multiple components at the same time.
const inputFields = document.getElementsByClassName('form-control');

//getElementsByTagName => can be used when targeting elements of the same tags.
const heading = document.getElementsByTagName('h3');

console.log(heading);
//check if you were able to successfully target an element from the document.
console.log(firstName);
console.log(lastName);
console.log(inputFields);

//check the type of data that we targeted from the document.
// console.log(typeof firstName);

//get the information from the input fields
//target the value property of the object
// console.log(firstName.value);
//Box model = CSS, Object = JS